import 'package:flutter/material.dart';

Column _buildButtonColumn(Color color, IconData icon, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Icon(
        icon,
        color: color,
      ),
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;

    Widget personalInformation = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          Container(
              child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Container()],
                ),
              ),
              Icon(
                Icons.favorite,
                color: Colors.red[600],
              ),
            ],
          )),
          Container(
              child: Text(
            'ข้อมูลส่วนตัว',
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("ชื่อ-สกุล: กาญจนาภรณ์ บุญประสิทธิ์"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("ชื่อเล่น: กระต่าย"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("เกิดวันอังคาร ที่ 15 กุมภาพันธ์ 2543"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("อายุ 21 ปี"),
              ),
            )
          ])),
          Container(
              child: Text(
            'การศึกษา',
            style: TextStyle(fontWeight: FontWeight.bold),
          )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text(
                  "ปัจจุบัน",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สถานศึกษา: มหาวิทยาลัยบูรพา"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("คณะ: วิทยาการสารสนเทศ"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สาขา: วิทยาการคอมพิวเตอร์"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text(
                  "มัธยมปลาย",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("สถานศึกษา: โรงเรียนขุนหาญวิทยาสรรค์"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("หลักสูตร: วิทย์ - คณิต"),
              ),
            ),
          ])),
          Container(
              padding: const EdgeInsets.all(32),
              child: Text(
                'เครื่องมือที่ใช้',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                    child: new Image.asset(
                  'images/visual_studio.png',
                  height: 40.0,
                  fit: BoxFit.cover,
                )),
                Container(
                    child: new Image.asset(
                  'images/vue.png',
                  height: 60.0,
                  fit: BoxFit.cover,
                )),
                Container(
                    child: new Image.asset(
                  'images/java.png',
                  height: 60.0,
                  fit: BoxFit.cover,
                )),
                Container(
                    child: new Image.asset(
                  'images/gitLab.png',
                  height: 60.0,
                  fit: BoxFit.cover,
                )),
              ],
            ),
          ),
          Container(
              padding: const EdgeInsets.all(32),
              child: Text(
                'งานอดิเรก',
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
          Container(
              child: Column(children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- ชอบฟังเพลง"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- เล่นโทรศัพท์มือถือ"),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                child: Text("- เล่นกับแมวที่ห้อง"),
              ),
            ),
          ])),
        ],
      ),
    );
    Widget buttonEducation = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.email, 'EMAIL'),
          _buildButtonColumn(color, Icons.facebook, 'FACEBOOK'),
        ],
      ),
    );
    return MaterialApp(
        title: 'Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('RESUME'),
          ),
          body: ListView(children: [
            Image.asset('images/Me.jpg',
                width: 600, height: 350, fit: BoxFit.cover),
            personalInformation,
            buttonEducation,
          ]),
        ));
  }
}
